const fs = require("fs");

function question1() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);
            let result = [];
            data.employees.filter((employee) => {
                if (
                    employee.id === 2 ||
                    employee.id === 13 ||
                    employee.id === 23
                ) {
                    result.push(employee);
                }
            });
            fs.appendFile("output1.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output1.json");
                    question2();
                }
            });
        }
    });
}

function question2() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);
            let result = data.employees.reduce((acc, emp) => {
                if (acc[emp.company]) {
                    acc[emp.company].push(emp);
                } else {
                    acc[emp.company] = [emp];
                }
                return acc;
            }, {});

            fs.appendFile("output2.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output2.json");
                    question3();
                }
            });
        }
    });
}

function question3() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);

            let result = data.employees.reduce((acc, emp) => {
                if (emp.company.toLowerCase() === "powerpuff brigade") {
                    acc.push(emp);
                }
                return acc;
            }, []);

            fs.appendFile("output3.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output3.json");
                    question4();
                }
            });
        }
    });
}

function question4() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);

            let result = data.employees.filter((emp) => {
                if (emp.id !== 2) {
                    return emp;
                }
            });

            fs.appendFile("output4.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output4.json");
                    question5();
                }
            });
        }
    });
}

function question5() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);

            data.employees.sort((emp1, emp2) => {
                if (emp1.company.toLowerCase() > emp2.company.toLowerCase()) {
                    return 1;
                } else if (
                    emp1.company.toLowerCase() < emp2.company.toLowerCase()
                ) {
                    return -1;
                } else {
                    return emp1.id - emp2.id;
                }
            });

            fs.appendFile("output5.json", JSON.stringify(data), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output5.json");
                    question6();
                }
            });
        }
    });
}

function question6() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);

            let company92;
            data.employees.filter((emp) => {
                if (emp.id === 92) {
                    company92 = emp.company;
                }
            });

            let company93;
            data.employees.filter((emp) => {
                if (emp.id === 93) {
                    company93 = emp.company;
                }
            });

            let result = data.employees.filter((emp) => {
                if (emp.id === 92) {
                    emp.company = company93;
                    return emp;
                } else if (emp.id === 93) {
                    emp.company = company92;
                    return emp;
                }
            });

            fs.appendFile("output6.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output6.json");
                    question7();
                }
            });
        }
    });
}

function question7() {
    fs.readFile("data.json", "utf-8", (err, data) => {
        if (err) {
            console.error("Error: ", err);
        } else {
            data = JSON.parse(data);

            let result = data.employees.map((emp) => {
                if (emp.id % 2 == 0) {
                    const dt = new Date();
                    emp.birthday = `${dt.getDate()}/${
                        dt.getMonth() + 1
                    }/${dt.getFullYear()}`;
                }
                return emp;
            });

            fs.appendFile("output7.json", JSON.stringify(result), (err) => {
                if (err) {
                    console.error("Error: ", err);
                } else {
                    console.log("Data added in output7.json");
                }
            });
        }
    });
}

question1();

